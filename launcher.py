#!/usr/bin/env python3
import sys

from PySide import QtGui

from obj_viewer.main_window import MainWindow


application = QtGui.QApplication(sys.argv)
main = MainWindow()
main.show()
sys.exit(application.exec_())
