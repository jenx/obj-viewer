from PySide import QtCore
from PySide import QtGui

from obj_viewer.constants import DISTANCE
from obj_viewer.errors import WrongFileFormatError
from obj_viewer.matrices import (Matrix, ViewportTransformation,
                                 Point, Identity, Rotation,
                                 Translation, Scaling)
from obj_viewer.face import Face


class Model(object):
    """Main class handling model creation and manipulation.

    Key attributes:
    canvas      -- the scene the model is painted on
    vertices    -- model's vertices are recorded in this list as
                   instances of the Point class
    faces       -- a list containing Faces (lists) of vertices
                   (Points) joined by an edge
    """

    def __init__(self, canvas, filename, environment):
        self.vertices = []
        self.faces = []
        self.environment = environment
        self.load_from_file(filename)
        self.canvas = canvas
        self.environment.mod = Identity()

    def load_from_file(self, filename):
        """Load the object from an .obj file. Only examine vertices
        ('v') and faces ('f'). Reindex the vertices so that they start
        at 0, not at 1.

        If any error occurs (IOError while opening the file, wrong
        file format), propagate it so that it can be taken care of in
        the right context.
        """

        try:
            source = open(filename)
        except IOError:
            sys.stderr.write('There was a problem opening the input file.')
            raise
        for line in source:
            # Load all the vertices:
            if line.startswith('v'):
                try:
                    self.vertices.append(Point(*[float(coordinate) for
                                                 coordinate in
                                                 line[2:].rstrip('\n').split()]))
                except TypeError:
                    raise WrongFileFormatError('Invalid input file - '
                                               'unexpected file '
                                               'format.')
                    return

            # Load the faces:
            elif line.startswith('f'):
                try:
                    self.faces.append(Face([self.vertices[int(index) - 1] for
                                            index in
                                            line[2:].rstrip('\n').split()],
                                           self.environment))
                except IndexError:
                    raise WrongFileFormatError('Invalid input file - '
                                               'face defined before '
                                               'all of its vertices.')
                    return
                except TypeError:
                    raise WrongFileFormatError('Invalid input file - '
                                               'unexpected file '
                                               'format.')
                    return

        print('A total of %d vertices and %d faces has been loaded.'
              % (len(self.vertices), len(self.faces)))


    def transform(self, transformation):
        self.environment.mod *= transformation
        self.render()

    def reset(self):
        self.environment.mod = Identity()
        self.render()

    def rotate(self, **kwargs):
        kwargs['clockwise'] = self.environment.rotation_inverted
        matrix = Rotation(**kwargs)
        self.transform(matrix)

    def translate(self, **kwargs):
        if self.environment.translation_inverted:
            kwargs['dist'] = -DISTANCE
        matrix = Translation(**kwargs)
        self.transform(matrix)

    def scale(self, **kwargs):
        matrix = Scaling(**kwargs)
        self.transform(matrix)

    def render(self):
        self.canvas.clear()
        mod = self.environment.mod
        view = self.environment.view_matrix
        for face in self.faces:
            if face.is_visible():
                color = face.calculate_color()
                polygon = QtGui.QPolygonF()
                for vertex in face:
                    x, y, z = (vertex * mod * view).to_point().get_xyz()
                    polygon.append(QtCore.QPointF(x, y))
                x, y, z = (face[0] * mod * view).to_point().get_xyz()
                polygon.append(QtCore.QPointF(x, y))
                if self.environment.wire:
                    pen = QtGui.QPen("black")
                else:
                    pen = QtGui.QPen(color)
                self.canvas.addPolygon(polygon, pen, QtGui.QBrush(color))
