from PySide.QtGui import QColor

from obj_viewer.constants import CAMERA
from obj_viewer.matrices import Point, Vector

class Face(list):

    def __init__(self, vertices, environment):
        super(Face, self).__init__(vertices)
        self.environment = environment

    def find_center(self):
        """Returns a point inside the face."""
        x, y, z = 0, 0, 0
        for vertex in self:
            x0, y0, z0 = vertex.get_xyz()
            x += x0
            y += y0
            z += z0
        return Point(x / len(self), y / len(self), z / len(self))

    def find_normal(self):
        """Takes two vectors belonging to the face's plane and
        calculates their cross product to get a vector perpendicular
        to the plane the face is on.
        """
        mod = self.environment.mod
        v1 = (self[1] * mod).to_point() - (self[0] * mod).to_point()
        v2 = (self[2] * mod).to_point() - (self[1] * mod).to_point()
        return (v1.cross_product(v2)).to_vector()

    def is_visible(self):
        """Checks if the cosine of the angle between the face and
        the camera's line of view is positive.
        """
        normal = self.find_normal()
        return normal.dot_product(Vector(*CAMERA)) > 0

    def calculate_intensity(self):
        """Calculate the resulting intensity (0-1) of the light
        reflected by the face based on the dot product of the
        face's normal and the ray (vector) emitted from the light
        source towards the face.
        """
        v1 = self.find_normal()
        v2 = (self.environment.light - self.find_center()).to_vector()
        result = v1.dot_product(v2)
        if result <= 0:
            return 0
        else:
            return result

    def calculate_color(self):
        full = self.environment.color
        intensity = self.calculate_intensity()
        return QColor(full.red() * intensity,
                      full.green() * intensity,
                      full.blue() * intensity)
